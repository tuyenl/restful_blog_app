

# Edit/Update
* Add Edit Route
* Add Edit Form
* Add Update Route
* Add Update Form
* Add Method-Override for put request in edit form
    action = "/blogs/<%= blogs._id %>?_method=PUT" will ovveride the POST request 

To update after submitting edit form
In the update route there is method
Blog.findByIdAndUpdate(id, newdata, callback)


# Destroy
 * Add Destroy Route
 * Add Edit and Destroy Links 


## Final Updates
* Sanitize blog body (removes scripts from possible hacking)
    - Is used for routes that save data to database
    - npm install --save express-santizer
    - then expressSanitizer = require("express-sanitizer");
    - then use it so app.use(expressSanitizer());
    - in the POST/PUT routes you will need to santize the body so in this instance it is req.body.blog.body = req.sanitize(req.body.blog.body);
--remove scripts from Create and 
* Style Index
* Update REST Table