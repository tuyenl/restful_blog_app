var express = require('express'), 
    app = express(),
    bodyParser = require('body-parser'), 
    mongosee = require('mongoose'),
    methodOverride = require('method-override'),
    expressSanitizer = require('express-sanitizer');

mongosee.connect("mongodb://localhost/restful_blog_app", {useNewUrlParser:true, useUnifiedTopology: true});
app.use(express.static("public")); // to use customer css stylesheet
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());
app.use(methodOverride("_method"));
var blogSchema = new mongosee.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now()} // automatically uses current date user creates blog without user specifying
})

var Blog = mongosee.model("blog", blogSchema);

// Create a blog for testing
/* Blog.create({
    title: "Cute Dog",
    image: "https://images.unsplash.com/photo-1531842477197-54acf89bff98?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60",
    blog: "The cutest dog in the world, with fluffy fur and beatiful smile"

}); */

//RESTful Routes

app.get("/", (req, res) =>{
    res.redirect("/blogs");
})

//INDEX ROUTE
app.get("/blogs", (req, res)=>{
    Blog.find({}, (err, allblogs)=>{
        if(err){
            console.log(err);
        }
        else{
            res.render("index", {blogs: allblogs})
        }
    })
    
})

//NEW ROUTE
app.get("/blogs/new", (req,res)=>{
    res.render("new");
})



//CREATE ROUTE
app.post("/blogs", (req,res)=>{
    console.log(req.body);
    req.body.blog.body = req.sanitize(req.body.blog.body); // removes scripts from blog body
    console.log("==================================================")
    console.log(req.body);
    Blog.create(req.body.blog, (err, newBlog)=>{
        if(err){
            console.log(err);
        }
        else{
            res.redirect("/blogs")
        }
    })


})

//SHOW ROUTE
app.get("/blogs/:id", (req, res)=> {
    Blog.findById(req.params.id, (err, foundBlog)=>{
        if(err){
            res.redirect("/blogs")
        }
        else{
            res.render("show",{blog: foundBlog});
        }
    })

});


//EDIT ROUTE
app.get("/blogs/:id/edit", (req, res)=>{
    Blog.findById(req.params.id, (err,foundBlog)=>{
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.render("edit",{blogs: foundBlog})
        }
    })
    
});

//UPDATE ROUTE
app.put("/blogs/:id" ,(req, res)=>{
    console.log(req.body);
    req.body.blog.body = req.sanitize(req.body.blog.body);
    console.log("================")
    console.log(req.body);
    Blog.findByIdAndUpdate(req.params.id, req.body.blog,(err, updatedBlog)=>{
        if(err){
            res.redirect("/blogs")
        }
        else{
            res.redirect("/blogs/" + req.params.id);
        }
    })
})

//DELETE ROUTE
app.delete("/blogs/:id" , (req,res)=>{
    Blog.findByIdAndRemove(req.params.id, (err)=>{
        if(err){
            res.redirect("/blogs");
        }
        else{
            res.redirect("/blogs");
        }
    })

})

app.listen(3000, ()=>{
    console.log("Server is running");
})